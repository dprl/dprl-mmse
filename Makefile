# Define environment, bash script execution line
all: install

clean-full:
	./bin/clean-full

install:
	./bin/install

rebuild: clean-full install

resources-up:
	./bin/resources-up

download-1996:
	./bin/acl/get-acl-1996-data

index-all:
	./bin/acl/load-acl $(configdir)

server-up:
	./bin/server-up $(configdir)

resources-down:
	./bin/resources-down


