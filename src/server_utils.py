import os.path
from glob import glob
from os import path
from typing import List, Dict, Tuple
import lxml.etree as ET
from bs4 import BeautifulSoup
from idna import unicode

from phocindexing.anyphoc.protables.ProTables import read_pro_tsv

src_dir = os.path.dirname(os.path.realpath(__file__))
xslt = ET.parse(os.path.join(src_dir, "..", "xsl", "mmltex.xsl"))
transform = ET.XSLT(xslt)


def get_region_bounding_box_from_pro(pro_list, doc_name, page, doc_region) -> Tuple[Tuple[int, int, int, int], str]:
    pro = get_pro_table_by_file_name(pro_list, doc_name)
    mml = 'No MathML available'

    for i in pro[page][doc_region][1]:
        # there should only be one mml object in a region but there is not a guarauntee as to which one it will be
        if i[5] == 'MML':
            mml = i[4]
    region = pro[page][doc_region][0]
    # convert to width and height
    width = region[2] - region[0]
    height = region[3] - region[1]
    new_region = (region[0], region[1], width, height)

    return new_region, mml


def get_all_bounding_box_from_pro(pro_list, doc_name, page) -> List[Dict]:
    pro = get_pro_table_by_file_name(pro_list, doc_name)

    regions = []
    for region in pro[page]:
        region_bbox = region[0]
        width = region_bbox[2] - region_bbox[0]
        height = region_bbox[3] - region_bbox[1]
        region_bbox = (region_bbox[0], region_bbox[1], width, height)
        # there should only be one mml object in a region but there is not a guarauntee as to which one it will be
        mml = 'No MathML available'
        latex = "No Latex available"
        for ob in region[1]:
            if ob[5] == 'MML':
                mml = ob[4].replace('\n', "").replace('\\n', "")
                latex = convert_mml_to_tex(mml)
        regions.append({"region": region_bbox, "mml": mml, "latex": latex})

    return regions


def read_all_pro_tables(tsv_dir):
    tsv_files = sorted(glob(path.join(tsv_dir, "*.tsv"), recursive=True))
    pro_table_list = {}
    for t in tsv_files:
        # Read in PRO table
        (pro_table, next_doc_page_sizes) = read_pro_tsv(t)
        pro_table_list[os.path.split(pro_table[0][0].strip("\n"))[1]] = pro_table[0][1]

    return pro_table_list


def get_pro_table_by_file_name(pro_list: Dict, file_name: str) -> List:
    for full_name in pro_list.keys():
        if os.path.split(full_name)[1] == file_name:
            return pro_list[full_name]


def merge_result_dicts(lod: List[Dict]) -> Dict:
    final_dict = {}
    for d in lod:
        final_dict = {**final_dict, **d}
    return final_dict


def group_by_doc_page(results: List[Dict], pro, collection, text_data, bib_data) -> Dict:
    doc_dict_list: Dict = {}
    merged = merge_result_dicts(results)
    for source in merged:
        for hit in merged[source]:
            add_hit_to_doc_page_map(doc_dict_list, hit, source, pro, collection, text_data)

    for doc in doc_dict_list:
        cur_doc = doc_dict_list[doc]
        doc_name_no_ext = os.path.splitext(doc)[0]
        bib_tex_dict = bib_data[doc_name_no_ext]
        # bib_tex_dict = bib_data[bib_data["docno"] == doc_name_no_ext].to_dict()
        # add bibtex stuff
        cur_doc["bibtex"] = {
            "bibtype": bib_tex_dict["bibtype"][doc_name_no_ext]
        }
        for k in bib_tex_dict:
            if bib_tex_dict[k][doc_name_no_ext] != 'None':
                cur_doc["bibtex"][k] = bib_tex_dict[k][doc_name_no_ext]
        pages = cur_doc["pages"]
        pages_scores = [pages[p]["score"] for p in pages]

        doc_dict_list[doc]["score"] = max(pages_scores)
    return doc_dict_list


def add_hit_to_doc_page_map(doc_dict_list, hit, source, pro, collection, text_data):
    doc = hit["doc"]
    doc_no_pdf = doc
    if os.path.splitext(doc)[1] == "":
        doc = doc + ".pdf"
        hit["doc"] = doc
    else:
        doc_no_pdf = os.path.splitext(doc)[0]
    if doc not in doc_dict_list:
        doc_dict_list[doc] = {
            "doc": doc,
            "collection": collection,
            "pages": {},
            "total_pages": text_data[doc_no_pdf]["__total_pages__"],
            "score": 0
        }

    add_hit_to_doc_page(doc_dict_list[doc]["pages"], hit, pro, source)


def strip_atts_and_convert(mathml) -> str:
    full_soup = BeautifulSoup(mathml, "lxml")
    soup = full_soup.find("math")
    for tag in soup.find_all():
        try:
            deletes = list(tag.attrs.keys())
            for key in deletes:
                del tag[key]
        except AttributeError:
            pass
    return str(soup)


def convert_mml_to_tex(mml: str) -> str:
    """
    code modified from https://github.com/oerpub/mathconverter
    """
    mathml = f'<math xmlns="http://www.w3.org/1998/Math/MathML">{mml[1:len(mml) - 1]}</math>'
    dom = ET.fromstring(strip_atts_and_convert(mathml))
    newdom = transform(dom)
    return unicode(newdom).strip(" $")


def add_hit_to_doc_page(pages_dict: Dict, hit: Dict, pro, source):
    doc = hit["doc"]
    page = int(hit["page"])

    score = hit["score"]
    if "region" in hit:
        region = hit["region"]
        highlight, tool_tip = get_region_bounding_box_from_pro(pro, doc, page, region)
        tool_tip = tool_tip.replace('\n', "").replace('\\n', "")
        latex = convert_mml_to_tex(tool_tip)
        highlights = {"math__ml": [highlight]}
    else:
        latex = None
        score = score + 1
        tool_tip = hit["query"]
        highlights = hit["highlights"]
    print(page)
    if page not in pages_dict:
        pages_dict[page] = {
            "page": page,
            "doc": doc,
            "hits": [],
            "score": 0
        }
    pages_dict[page]["score"] = pages_dict[page]["score"] + score

    hit = {
        "score": score,
        "page": page,
        "tooltip": tool_tip,
        "index": source,
        "highlights": highlights
    }
    if latex is not None:
        hit["latex"] = latex
    pages_dict[page]["hits"].append(hit)


def sort_results(text_and_phoc_results: Dict, sort_type: str, offset: int) -> Dict:
    if sort_type == "score":
        sorted_tuple_list = sorted(text_and_phoc_results.items(), key=lambda x: x[1]["score"], reverse=True)
        offset_applied_tuple_list = sorted_tuple_list[offset:]
        return dict(offset_applied_tuple_list)

    years = {}
    for document in text_and_phoc_results:
        doc_dict = text_and_phoc_results[document]
        doc_year = doc_dict["bibtex"]["year"]
        years_list: List = years.get(doc_year, [])
        years_list.append(doc_dict)
        years[doc_year] = years_list

    if sort_type == "ascending":
        sort_reverse = False
    else:  # descending
        sort_reverse = True
    sorted_years = sorted(years.items(), key=lambda x: int(x[0]), reverse=sort_reverse)
    grouped_by_sorted_doc_list = []
    for year in sorted_years:
        score_sorted_year: List = sorted(year[1], key=lambda x: x["score"], reverse=True)
        year_tuples: List[Tuple] = [(doc_dict["doc"], doc_dict) for doc_dict in score_sorted_year]
        grouped_by_sorted_doc_list.extend(year_tuples)

    offset_grouped_by_sorted_doc_list = grouped_by_sorted_doc_list[offset:]
    return dict(offset_grouped_by_sorted_doc_list)
