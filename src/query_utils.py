from typing import Tuple, List

MATH_DELIMITER = "<$>"


def parse_query_str(query_str) -> Tuple[str, List[str]]:
    # returns a new query string for text and a list of latex maths
    is_txt = True
    text_query_str = ""
    maths = []
    while len(query_str) > 0:
        start = query_str.find(MATH_DELIMITER)
        if start == -1:
            text_query_str = text_query_str + query_str            
            break
        elif is_txt:
            text_query_str = text_query_str + query_str[:start]
            query_str = query_str[start + len(MATH_DELIMITER):]
            is_txt = False
        else:
            math = query_str[:start]
            if len(math) > 0:
                maths.append(math)
            query_str = query_str[start + len(MATH_DELIMITER):]
            is_txt = True

    return text_query_str, maths
