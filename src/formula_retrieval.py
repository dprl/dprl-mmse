################################################################
# formula_retrieval.py
#
# Functions for invoking external formula search engines.
#
# Authors: Matthew Langesnkamp, R. Zanibbi, June 2022
################################################################

from opensearchpy import OpenSearch, helpers
# from mathfire.ES_module.retrieval_tools import CFTInterface
# from mathfire.ES_module.search import SearchViaES
from phocindexing.opensearchutils.idf import get_idf_map
from phocindexing.opensearchutils.run_query import run_query
from phocindexing.phocdefs import phoc_defs
from phocindexing.anyphoc.canvas.converters.svgcanvas import render
from phocindexing.opensearchutils.jsonquery import build_json_query
from typing import List, Dict
import asyncio
import aiohttp
from aiohttp import ClientSession


async def search_phoc_2(index_name, query_latex) -> Dict:
    pass


def search_phoc(opensearch_client, index_name, query_latex, phoc_def="R9"):
    render_url = 'http://127.0.0.1:8045/render/'
    retrieval_func = ""
    svg_data = render(query_latex, render_url)
    compose = phoc_defs[phoc_def]()
    vector_length = compose.explain()[1]
    query = build_json_query(
        svg_data, compose, vector_length, retrieval_func, "0%", "line", 1, "normal", compose.make_mask(), mask_op="mul")
    hits = run_query(opensearch_client, index_name, query, num_hits=19)
    return hits


if __name__ == "__main__":
    # (Test require search services to be available!)
    # Test 1: Call PHOC search engine with 3 examples, different params
    #         Print results to terminal
    #
    test_q1 = r"\int_0^{ \sqrt{ \frac{ \sqrt{n} }{n} } }"
    test_q2 = r"z=\sqrt[n]{s}e^{\frac{i\varphi}{n}}"
    test_q3 = r"C"

    auth = ('admin', 'admin')
    url = "localhost"
    port = 9200
    client = OpenSearch(
        hosts=[{'host': url, 'port': port}],
        http_compress=True,  # enables gzip compression for request bodies
        http_auth=auth,
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    q1 = search_phoc(client, "xy5pro-acl5", test_q1)
    q2 = search_phoc(client, "xy5pro-acl5", test_q2)
    q3 = search_phoc(client, "xy5pro-acl5", test_q3)
    print("\n----------\nPhoc\n----------\n")
    if len(q1) > 1:
        print(len(q1), q1[0])
    else:
        print("no hits for q1")

    if len(q2) > 1:
        print(len(q2), q2[0])
    else:
        print("no hits for q2")

    if len(q3) > 1:
        print(len(q3), q3[0])
    else:
        print("no hits for q3")
