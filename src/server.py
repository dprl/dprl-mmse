import json
import os
import tempfile
from io import BytesIO
from typing import List
from copy import copy
import bs4
import pandas as pd
from src.query_utils import parse_query_str
from src.server_utils import get_all_bounding_box_from_pro, read_all_pro_tables, get_region_bounding_box_from_pro, \
    group_by_doc_page, sort_results
import pypdf
from urllib.parse import unquote
from pypdf import PdfWriter
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, HTTPException, File
from opensearchpy import OpenSearch
from starlette.responses import FileResponse
import uvicorn
import aiohttp
from src.formula_retrieval import search_phoc
from src.text_location_mapping import merge_in_location_data, merge_in_phoc, accumulate_scores_and_add_bib
from configparser import ConfigParser, ExtendedInterpolation

config = ConfigParser(interpolation=ExtendedInterpolation())
config.read(os.environ['CONFIG_PATH'])

phoc_index_name = config.get('index', 'PHOC_INDEX_NAME')
text_index_name = config.get('index', 'TEXT_INDEX_NAME')
collection_name = config.get('api', 'COLLECTION_NAME')

app = FastAPI()
alpha = 3
print("loading pro data")
pro_table_list = read_all_pro_tables(f"./data/acl/{collection_name}/**/")
print("loading bib data...")
with open('bibtexmap.json') as json_file:
    bib_data = json.load(json_file)
# pd_all_bib = pd.read_pickle('@bib_data.pd')

origins = [
    "*",
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:8081",
    "https://rjb.cs.rit.edu",
    "https://dev.mathdeck.org",
    "https://mathdeck.org",
    "https://demo.mathdeck.org"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

auth = ('admin', 'admin')
url = "localhost"
port = 9200
client = OpenSearch(
    hosts=[{'host': url, 'port': port}],
    http_compress=True,  # enables gzip compression for request bodies
    http_auth=auth,
    use_ssl=True,
    verify_certs=False,
    ssl_assert_hostname=False,
    ssl_show_warn=False,
)


@app.get("/searchFull")
def search_full(collection: str, query_str: str = None, sort_type="score", offset: int = 0):
    try:
        query_str = unquote(query_str)
        print(query_str)
        new_query_str, maths = parse_query_str(query_str)
        resp = client.search(body={
            "size": 40,
            "query": {
                "query_string": {
                    "query": new_query_str,
                    "analyzer": "standard"
                }
            }
        }, index=text_index_name)

        analyzed = client.indices.analyze(body={
            "analyzer": "stop",
            "text": new_query_str
        }, index=text_index_name)

        query_tokens = [analyzed_token["token"] for analyzed_token in analyzed["tokens"]]
        phoc_results_unfiltered = get_phoc_results(maths)

        hits_unfiltered = resp["hits"]["hits"]
        text_location_data = {}

        hits = []
        for hit in hits_unfiltered:
            file_name = hit["_source"]["basefile"]
            try:
                f = open(os.path.join("data", "acl", collection_name, "loc_json", file_name + ".json"))
            except FileNotFoundError as e:
                print(e)
                print(f"could not find json loc file for {file_name}")
                continue
            text_location_data[file_name] = json.load(f)
            f.close()
            hits.append(hit)

        phoc_results = []
        for hit in phoc_results_unfiltered:
            file_name = hit["_source"]["doc"]
            try:
                f = open(os.path.join("data", "acl", collection_name, "loc_json", file_name + ".json"))
            except FileNotFoundError as e:
                print(e)
                print(f"could not find json loc file for {file_name}")
                continue
            text_location_data[file_name] = json.load(f)
            f.close()
            phoc_results.append(hit)

        text_results = merge_in_location_data(text_location_data, hits, query_tokens, collection_name)
        text_and_phoc_results = merge_in_phoc(phoc_results, pro_table_list, text_results, collection_name,
                                              text_location_data, alpha)
        accumulate_scores_and_add_bib(text_and_phoc_results, bib_data)
        results = sort_results(text_and_phoc_results, sort_type, offset)
        return results

    except Exception as e:
        print(e)
        raise HTTPException(status_code=404, detail="Server Error")


@app.get("/render-mathml")
async def render_mathml(mathml: str):
    async with aiohttp.ClientSession() as session:
        v = await session.post("http://localhost:8045/render-mathml", json={"mathml": mathml})
        c = await v.content.read()
        soup = bs4.BeautifulSoup(c, 'lxml')
        return str(soup.find('svg'))


def get_phoc_results(query_strs: List):
    results = []
    for query_str in query_strs:
        query_str = unquote(query_str)
        result = search_phoc(client, phoc_index_name, query_str)
        for r in result:
            r["matched_query"] = query_str
        results.extend(result)
    return results


@app.get("/getProData")
def get_pro_data(collection: str, doc_name: str, page_num: int, formula_region: int):
    try:
        return get_region_bounding_box_from_pro(pro_table_list, doc_name, page_num, formula_region)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=404, detail="collection not found")


@app.get("/getProDataWholePage")
def get_pro_page_data(collection: str, doc_name: str, page_num: int):
    try:
        return get_all_bounding_box_from_pro(pro_table_list, doc_name, page_num)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=404, detail="collection not found")


@app.get("/getPdfPage")
def get_pdf_page(collection: str, doc_name: str, page_num: int):
    # check if exists
    try:
        doc_name = doc_name.replace("_", ".")
        if os.path.splitext(doc_name)[1] == "":
            doc_name = doc_name + ".pdf"
        path = os.path.join("data", "acl", collection_name, "pdf", doc_name)
        if os.path.exists(path):
            sample_pdf = open(path, mode='rb')
            pdf_doc = pypdf.PdfReader(sample_pdf)

            page_needed = pdf_doc.pages[page_num]

            output = PdfWriter()
            output.add_page(page_needed)
            new_file_name = os.path.splitext(doc_name)[0] + f"_{str(page_num)}.pdf"
            output_stream = tempfile.NamedTemporaryFile(delete=False)
            output.write(output_stream)
            name = output_stream.name
            output_stream.close()
            return FileResponse(
                name,
                media_type='application/pdf',
                filename=new_file_name
            )
        else:
            print(f"could not find file at {path}")
            raise HTTPException(status_code=404, detail="collection not found")
    except Exception as e:
        raise HTTPException(status_code=404, detail="collection not found")


@app.get("/getPdfFull")
def get_pdf_full(collection: str, doc_name: str):
    sub_collection = "ACL-year-1952-2005"  # ML: hack, change this later

    doc_name = doc_name.replace("_", ".")
    if os.path.splitext(doc_name)[1] == "":
        doc_name = doc_name + ".pdf"
    path = os.path.join("data", "acl", collection_name, "pdf", doc_name)
    # check if exists
    if os.path.exists(path):
        return FileResponse(
            path,
            media_type='application/pdf',
            filename=os.path.basename(path)
        )
    else:
        raise HTTPException(status_code=404, detail="collection not found")


@app.post("/adHocIndex")
async def ad_hoc_index(pdf: bytes = File(...)):
    params = {'stride': '0.75', 'conf': '0.5', 'dpi': '256'}
    async with aiohttp.ClientSession() as session:
        file = {"file": copy(pdf)}
        async with session.post('http://0.0.0.0:8030/predict/', params=params, data=file) as resp:
            content = await resp.content.read()
            b = BytesIO(content)
            print(content)
            print(b)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
