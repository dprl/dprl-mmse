import gzip
import json
import os.path
from io import BytesIO
import bibtexparser
import requests
from tqdm import tqdm

if __name__ == '__main__':

    years = {}

    bib_gz = requests.get("https://aclanthology.org/anthology.bib.gz")
    bib = ""
    print("reading in ACL anthology bibtex file and making year map")
    for line in tqdm(gzip.open(BytesIO(bib_gz.content)).readlines()):
        decoded_line = line.decode()
        if "@inproceedings{" in decoded_line:
            bib = bib + decoded_line
        elif "}" == decoded_line.strip():
            bib = bib+decoded_line
            parsed = bibtexparser.loads(bib)
            entry = parsed.entries[0]
            year = entry['year']
            year_map = years.get(year, {})
            url_iden = entry['url'].split("/")[-1].replace(".", "_")
            year_map[url_iden] = entry
            years[year] = year_map
            bib = ""
        else:
            bib = bib + decoded_line

    with open("bibtexmap.json", "w") as saved:
        saved.write(json.dumps(years))

    # with open("bibtexmap.json", "r") as saved:
    #     years = json.loads(saved.read())

    os.makedirs("year_chunks", exist_ok=True)

    for year in years:
        year_txt_string = ""
        cur_year = years[year]
        for entry in cur_year:
            entry_dict = cur_year[entry]
            if ".pdf" in entry_dict["url"]:
                url = entry_dict["url"]
            else:
                url = entry_dict["url"]+".pdf\n"
            year_txt_string += url
        with open(os.path.join("year_chunks", f"{year}.txt"), "w") as txt_file:
            txt_file.write(year_txt_string)
