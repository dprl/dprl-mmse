import os.path
from typing import Dict

import numpy as np

from src.server_utils import get_region_bounding_box_from_pro, convert_mml_to_tex


def merge_in_location_data(location_data, response_list, query_tokens, collection):
    new_response_dict = {}

    for r in response_list:
        pdf_file = r["_source"]["basefile"] + ".pdf"
        page_num = r["_source"]["page_num"]
        file_dict = new_response_dict.get(pdf_file, {})
        if len(file_dict) == 0:
            file_dict = {
                "doc": pdf_file,
                "collection": collection,
                "pages": {},
                "total_pages": location_data[r["_source"]["basefile"]]["__total_pages__"],
                "score": 0
            }
        add_location_data_to_text_response(
            location_data, file_dict, query_tokens, r["_source"]["basefile"], page_num, r["_score"])

        new_response_dict[pdf_file] = file_dict

    return new_response_dict


def add_location_data_to_text_response(location_data, r, tokens, doc, page_num, score):
    cur_page = location_data[doc][str(int(page_num))]

    for q_token in tokens:
        if q_token in cur_page:
            for highlight in cur_page[q_token]:
                if True in np.isnan(highlight) or True in np.isinf(highlight):
                    return
            cur_page_map = r["pages"].get(page_num, {})
            cur_page_map["page"] = page_num
            cur_page_map["doc"] = doc + ".pdf"
            hits = cur_page_map.get("hits", [])
            hits.append({
                "tooltip": q_token,
                "index": "body_text",
                "page": page_num,
                "highlights": {
                    f"{q_token}": cur_page[q_token]
                },
                "score": score
            })
            cur_page_map["hits"] = hits
            cur_page_map["text_score"] = score
            r["pages"][page_num] = cur_page_map


def merge_in_phoc(response_list, pro, text_data, collection, location_data, alpha):
    for r in response_list:
        pdf_file = r["_source"]["doc"] + ".pdf"
        page_num = str(r["_source"]["page"])
        region = r["_source"]["region"]
        file_dict = text_data.get(pdf_file, {})
        if len(file_dict) == 0:
            file_dict = {
                "doc": pdf_file,
                "collection": collection,
                "pages": {},
                "total_pages": location_data[r["_source"]["doc"]]["__total_pages__"],
                "score": 0
            }
        add_location_data_to_graphics_response(pro, file_dict, pdf_file,
                                               page_num, region, r["_score"], r["matched_query"], alpha)
        text_data[pdf_file] = file_dict
    return text_data


def add_location_data_to_graphics_response(pro, r, doc, page_num, region, score, query_str, alpha):
    cur_page_map = r["pages"].get(page_num, {})
    cur_page_map["page"] = page_num
    cur_page_map["doc"] = doc
    hits = cur_page_map.get("hits", [])

    highlight, tool_tip = get_region_bounding_box_from_pro(pro, doc, int(page_num), region)
    tool_tip = tool_tip.replace('\n', "").replace('\\n', "")
    latex = convert_mml_to_tex(tool_tip)
    hits.append({
        "latex": latex,
        "tooltip": tool_tip,
        "matched_query": query_str,
        "index": "phoc",
        "page": page_num,
        "highlights": {
            f"math__ml": [highlight]
        },
        "score": score
    })
    cur_page_map["graphics_score"] = update_graphic_scores(cur_page_map.get(
        "graphics_score", {}), query_str, alpha * score)
    cur_page_map["hits"] = hits
    r["pages"][page_num] = cur_page_map


def update_graphic_scores(graphics_score_dict, query_str, score):
    cur_score = graphics_score_dict.get(query_str, 0)
    new_cur_score = max(cur_score, score)
    graphics_score_dict[query_str] = new_cur_score
    return graphics_score_dict


def get_sum_of_top_graphics_scores(graphics_dict: Dict) -> float:
    score_sum = 0
    for query_str in graphics_dict:
        score = graphics_dict[query_str]
        score_sum += score
    return score_sum


def accumulate_scores_and_add_bib(phoc_and_text_data, bib):
    for file_name in phoc_and_text_data:
        score = 0
        for page in phoc_and_text_data[file_name]["pages"]:
            page_dict = phoc_and_text_data[file_name]["pages"][page]
            graphics_score = get_sum_of_top_graphics_scores(page_dict.get("graphics_score", {}))
            page_score = graphics_score + page_dict.get("text_score", 0)
            page_dict["score"] = page_score
            score = max(score, page_score)

        phoc_and_text_data[file_name]["score"] = score
        phoc_and_text_data[file_name]["bibtex"] = find_bib(bib, os.path.splitext(file_name)[0])


def find_bib(bib, file_name):
    for year in bib:
        year_bib = bib[year]
        if file_name in year_bib:
            return year_bib[file_name]
