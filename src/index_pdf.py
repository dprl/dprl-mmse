import argparse
import dataclasses
import json
import multiprocessing
import os.path
from configparser import ConfigParser, ExtendedInterpolation
from typing import Dict, List

import numpy as np
from bs4 import BeautifulSoup
from opensearchpy import OpenSearch
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map

import math


@dataclasses.dataclass
class FnParams:
    bib_data: Dict
    index_name: str
    xml_dir_prefix: str


def index_document(params: FnParams):

    id_string = params.bib_data["ID"]
    url = params.bib_data["url"].split("/")
    url_file_iden = url[-1].replace(".", "_")
    sscraper_xml = os.path.join(params.xml_dir_prefix, url_file_iden+".xml")
    try:
        with open(sscraper_xml) as file_handle:
            doc_bulk_index_list = []
            XMLtree = BeautifulSoup(file_handle, 'lxml')
            pages = XMLtree.find_all('page')

            for page in pages:
                page_text = ""
                for line in page.find_all('line'):
                    for word in line.find_all('word'):
                        page_text = page_text + word.get_text().replace('\n', '') + ' '

                if "author" in params.bib_data:
                    author = params.bib_data["author"]
                else:
                    author = "Information Unavailable"

                if "publisher" in params.bib_data:
                    publisher = params.bib_data["publisher"]
                else:
                    publisher = "Information Unavailable"

                if page_text != '':
                    d = {
                        "text": page_text,
                        "title": params.bib_data["title"],
                        "author": author,
                        "publisher": publisher,
                        "year": params.bib_data["year"],
                        "page_num": str(int(page["id"])),
                        "docno": id_string,
                        "url": params.bib_data["url"],
                        "basefile": url_file_iden
                    }
                    doc_bulk_index_list.append({"index": {"_index": params.index_name, "_id": id_string+page["id"]}})
                    doc_bulk_index_list.append(d)
            return doc_bulk_index_list
    except FileNotFoundError as e:
        print(e)
        return []


def word_chars_to_bb(soup_node_list):
    # DEBUG: Some words have no characters in them.
    if len(soup_node_list) < 1:
        return [10000000, 100000000, -1, -1]

    # Compute bounding box from existing bounding boxes
    bbs = np.array(
        [list(map(float, node.get('bbox').split()))
         for node in soup_node_list])

    word_bb = (
        np.min(bbs[:, 0]), np.min(bbs[:, 1]),
        np.max(bbs[:, 2]+bbs[:, 0]), np.max(bbs[:, 3]+bbs[:, 1])
    )
    word_bb = (word_bb[0], word_bb[1], word_bb[2]-word_bb[0], word_bb[3]-word_bb[1])
    return word_bb


def append_to_word_dict(word_dict: Dict[str, List], word, bbox) -> Dict:
    if word in word_dict.keys():
        word_dict[word].append(bbox)
    else:
        word_dict[word] = [bbox]
    return word_dict


def extract_mapping(params: FnParams):
    auth_local = ('admin', 'admin')
    client_local = OpenSearch(
        hosts=[{'host': 'localhost', 'port': '9200'}],
        http_compress=True,  # enables gzip compression for request bodies
        http_auth=auth_local,
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    def tokenize(word_):
        analyzed = client_local.indices.analyze(body={
            "analyzer": "standard",
            "text": word_
        }, index=index_name)["tokens"]
        return [analyzed_token["token"] for analyzed_token in analyzed]

    def get_page_map(page) -> Dict[str, List]:
        word_map = {}
        for line in page.find_all('line'):
            for word in line.find_all('word'):
                tokens = tokenize(word.get_text().replace('\n', ''))
                pdf_bb = word_chars_to_bb(word.find_all('char'))
                for token in tokens:
                    word_map = append_to_word_dict(word_map, token, pdf_bb)
                pass
        return word_map

    page_map = {}
    url = params.bib_data["url"].split("/")
    url_file_iden = url[-1].replace(".", "_")
    sscraper_xml = os.path.join(params.xml_dir_prefix, url_file_iden+".xml")
    try:
        with open(sscraper_xml) as file_handle:
            xml_tree = BeautifulSoup(file_handle, 'lxml')
            pages = xml_tree.find_all('page')
            page_map["__total_pages__"] = len(pages)
            for i, cur_page in enumerate(pages):
                page_map[i] = get_page_map(cur_page)

        loc_json_dir = os.path.join(os.path.split(os.path.dirname(params.xml_dir_prefix.strip("/")))[0], "loc_json")
        os.makedirs(loc_json_dir, exist_ok=True)
        loc_json_file = os.path.join(loc_json_dir, f"{url_file_iden}.json")
        with open(loc_json_file, 'w+') as idf_file:
            idf_file.write(json.dumps(page_map, ensure_ascii=False, indent=4))
    except FileNotFoundError:
        pass


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config')
    args = parser.parse_args()
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(args.config)

    years_file_path = config.get('opensearch', 'YEARS_FILE_PATH')
    index_name = config.get('opensearch', 'TEXT_INDEX_NAME')
    overwrite_index = config.get('opensearch', 'OVERWRITE_INDEX') == "True"
    collection_name = config.get('opensearch', 'COLLECTION_NAME')

    with open(years_file_path) as years_file:
        years = years_file.readlines()
    auth = ('admin', 'admin')
    client = OpenSearch(
        hosts=[{'host': 'localhost', 'port': '9200'}],
        http_compress=True,  # enables gzip compression for request bodies
        http_auth=auth,
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    if client.indices.exists(index=index_name) and overwrite_index:
        print("not deleting indices right now")
        # client.indices.delete(index=index_name)
    if not client.indices.exists(index=index_name):
        client.indices.create(index=index_name, body={"settings": {"index": {
            "number_of_shards": 10,
            "number_of_replicas": 0,
            "refresh_interval": "120s"
        }}})
        client.indices.put_settings(
            index=index_name,
            body={
                "index.mapping.total_fields.limit": 4000
            }
        )
        client.indices.put_mapping(
            index=index_name,
            body={
                "properties": {
                    "text": {"type": "text"},
                    "title": {"type": "text"},
                    "author": {"type": "text"},
                    "publisher": {"type": "text"},
                    "pagenum": {"type": "integer"},
                    "year": {"type": "integer"},
                    "docno": {"type": "keyword"},
                    "url": {"type": "keyword"},
                    "basefile": {"type": "keyword"}
                }
            },
            request_timeout=10000)
    for year_n in years:
        year = year_n.strip()
        print(" ---------------------------- ")
        print(f"indexing year {year}")
        print(" ---------------------------- ")
        list_of_fn_params: List[FnParams] = []
        with open('bibtexmap.json') as json_file:
            bib_data = json.load(json_file)
            years_bib_data = bib_data[year.strip()]
            for doc_id in years_bib_data:
                doc = years_bib_data[doc_id]
                if doc["ENTRYTYPE"] != "proceedings":
                    list_of_fn_params.append(
                        FnParams(
                            bib_data=doc,
                            index_name=index_name,
                            xml_dir_prefix=f"data/acl/{collection_name}/{year}/xml/"
                        )
                    )

        print("Processing Text Documents to be loaded into OpenSearch")
        processed_documents = [bulk_index for bulk_index in process_map(index_document, list_of_fn_params)]
        print("Loading Documents into OS")
        for bulk_index in tqdm(processed_documents):
            if len(bulk_index) > 0:
                res = client.bulk(index=index_name, body=bulk_index, request_timeout=10000)
                for s in res['items']:
                    i = s['index']
                    if i["status"] != 200 and i["status"] != 201:
                        print("Failure!")
                        print(i)
                    elif not (i["result"] == "updated" or i["result"] == "created"):
                        print("Failure!")
                        print(i)

        workers = 10
        print(f"creating document-page-word bbox mapping with {workers} workers")

        process_map(extract_mapping, list_of_fn_params, max_workers=workers)
