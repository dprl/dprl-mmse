## DPRL Lab's Multi-Modal Search Engine

A multimodal search engine capable of indexing PDF text and graphics data. Currently, the system is capable of 
indexing PDFs from the [ACL Anthology](https://aclanthology.org/) dataset

## Installation and Quickstart


This code has been tested on MacOS X and Linux, and requires a bash command shell (i.e., command line). 

1. Make sure the Conda package management system is installed [Anaconda](https://www.anaconda.com/products/distribution)
2. Make sure Docker is in installed [Docker](https://docs.docker.com/get-docker/)
3. Run `docker swarm init` to make sure docker is in swarm mode. If you are already in swarm mode you do not need to run this command
4. Make sure you are compliant with the following settings in order to run opensearch: `https://opensearch.org/docs/1.3/opensearch/install/important-settings/`
5. If possible, make sure to install lxml, e.g., on Ubuntu, using: `sudo apt-get install python3-lxml`
6. make sure pcre is installed with `sudo apt-get install libpcre3 libpcre3-dev`
7. Run `make install` to download DPRL dependencies and create a conda env
8. Run `make resources-up` to start an opensearch cluster as well as spin up a mathjax server, if there is an Opensearch
instance already running it will throw an error claiming that the port is already in use, this error can be ignored
9. Run `make download-1996` to download data
10. Run `make index-all configdir=data/configs/ACL-year-1996`

11. To start the DPRL-MMSE server run `make server-up configdir=data/configs/ACL-year-1996`
12. go to `http://localhost:8000/docs` to see the swagger api documentation. You should be able to make a query as shown below. Press ctrl-C to shut down server ![alt text](swagger-req.png "Title")
13. to shut down resources run `make resources-down`

## Indexing New Data

Building graphics and text indices depends on three sources of information:
PROTables (graphics), SymbolScraper XML (Text), and the corresponding PDFs (visualization). 
These resources can be generated using our [Graphics Extraction Pipeline](https://gitlab.com/dprl/graphics-extraction)

For easy testing we provide pre-extracted PROTables, XML and PDFs for the year of 1996
These files can be downloaded by running `make download-1996`. Similarly, we provide 
a command for indexing this downloaded data `make index configdir=data/configs/ACL-year-1996`

To index your own data you will need to populate 2 directories: `data/acl/<name of your experiment>` 
and `data/configs/<name of your experiment>`.

Within `data/acl/<name of your experiment>` There will be any amount of "years" folders. For example the 1996 data example 
has a single year folder of `data/acl/ACL-year-1996/1996`
Within each year folder there must be 2 sub-folders, one populated with SymbolScraper xml outputs called `xml`, 
one with the PROTable outputs called `tsv`. Lastly there needs to be a folder called `pdf` which contains the PDFs for all years. 
For example `data/acl/ACL-year-1996/pdf`.
The base file names (filename without the extension) in these folders must align.

Within `data/configs/<name of your experiment>` There must be 
4 files: `years`, `server.ini`, `text.ini` and `phoc.ini`. `years` is just a text 
file with each line referring to a single year from the `data/acl/<name of your experiment>` directory.
`server.ini`, `text.ini`, and `phoc.ini` contain the necessary configuration 
information to index graphics and text information. Copy and modify the files from `data/configs/ACL-year-1996/` 
as needed. 

IMPORTANT NOTE on `phoc.ini`: This file is run from within the `phocindexing` directory, 
so you will need to preface the path for the `PRO_PATH` field with `../` to load in data from the root directory.
## MathDeck services
Mathdeck is currently serving a `dev` environment and `demo` environment. The frontends are being served via netlify.
To start these services open two different terminals run `./bin/server-up data/configs/ACL-year-1952-2005/server.ini` and 
`./bin/server-up data/configs/ACL-year-1952-2005-dev/server.ini`


## Authors and License

**Authors:** Richard Zanibbi (<rxzvcs@rit.edu>) and Matt Langsenkamp (<ml2513@rit.edu>)    

